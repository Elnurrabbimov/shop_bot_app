<?php

namespace backend\controllers;

use common\models\ShopProducts;
use common\models\search_models\ShopProductsSearch;
use common\models\Shops;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopProductsController implements the CRUD actions for ShopProducts model.
 */
class ShopProductsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ShopProducts models.
     *
     * @return string
     */
    public function actionIndex($shop_id)
    {
        $searchModel = new ShopProductsSearch();
        $searchModel->shop_id = $shop_id;
        $dataProvider = $searchModel->search($this->request->queryParams);

        $shop = $this->findShopModel($shop_id);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'shop' => $shop
        ]);
    }

    /**
     * Displays a single ShopProducts model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'shop' => $this->findShopModel($this->findModel($id)->shop_id)
        ]);
    }

    /**
     * Creates a new ShopProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($shop_id)
    {
        $shop = $this->findShopModel($shop_id);
        $model = new ShopProducts();
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if ($model->products) {
                    foreach ($model->products as $product_id) {
                        $shopProductModel = new ShopProducts();
                        if (ShopProducts::find()->where(['shop_id' => $shop_id, 'product_id' => $product_id])->exists()) {
                            continue;
                        } else {
                            $shopProductModel->shop_id = $shop_id;
                            $shopProductModel->product_id = $product_id;
                            $shopProductModel->save();
                        }

                    }
                }
                return $this->redirect(['index', 'shop_id' => $shop_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'shop' => $shop
        ]);
    }

    /**
     * Updates an existing ShopProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShopProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index', 'shop_id' => $model->shop_id]);
    }

    /**
     * Finds the ShopProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ShopProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopProducts::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Маълумотлар топилмади!');
    }

    protected function findShopModel($id)
    {
        if (($model = Shops::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Дўкон мавжуд эмас!');
    }
}
