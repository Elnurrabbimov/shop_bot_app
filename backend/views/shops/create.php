<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Shops */

$this->title = 'Янги қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Дўконлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">

    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>