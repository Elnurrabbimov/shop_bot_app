<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search_models\ShopsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дўконлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shops-index">

    <p>
        <?= Html::a('Янги қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'dataTables_wrapper dt-bootstrap4',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover dataTable dtr-inline',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'description:ntext',
//            'location',
            [
                'attribute' => 'logo',
                'value' => function ($model) {
                    return $model->logo ? Html::img($model->fileUrl, ['download' => true,'style' => ['width' => '200px']]) : '--';
                },
                'format' => 'raw',
                'filter' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info"><i class="fas fa-eye"></i></span>', $url, [
                            'title' => 'Дўконни кўриш',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-primary"><i class="fas fa-pencil"></i></span>', $url, [
                            'title' => 'Дўконни таҳрирлаш',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="btn btn-danger"><i class="fas fa-trash"></i></span>', $url, [
                            'title' => 'Дўконни ўчириш',
                            'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
