<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Shops */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дўконлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<link rel="stylesheet" href="/leaflet/leaflet.css"/>
<script src="/leaflet/leaflet.js"></script>

<div>
    <p>
        <?= Html::a('Таҳрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Дўконда мавжуд махсулотлар', ['/shop-products/index', 'shop_id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'description:ntext',
            [
                'attribute' => 'location',
                'value' => function ($model) {
                    return '<div id="map" style="width:100%; height:350px"></div>';
                },
                'format' => 'raw',
                'visible' => $model->longitude ? true : false,
            ],
            [
                'attribute' => 'logo',
                'value' => function ($model) {
                    return $model->logo ? Html::img($model->fileUrl, ['download' => true, 'style' => ['width' => '600px']]) : '--';
                },
                'format' => 'raw'
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ]
    ]); ?>
</div>


<script>
    if (<?= $model->latitude ? 'true' : 'false' ?>) {
        var mapOptions = {
            center: [<?= $model->latitude ?>, <?= $model->longitude ?>],
            zoom: 18
        }
        var map = new L.map('map', mapOptions);

        var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

        map.addLayer(layer);

        var marker = L.marker([<?= $model->latitude  ?>, <?= $model->longitude ?>]);

        marker.addTo(map);
    }
</script>