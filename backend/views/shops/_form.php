<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Shops */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" href="/leaflet/leaflet.css"/>
<script src="/leaflet/leaflet.js"></script>


<div class="shops-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <?= $form->field($model, 'longitude')->hiddenInput(['maxlength' => true, 'id' => 'longitude'])->label(false) ?>
    <?= $form->field($model, 'latitude')->hiddenInput(['maxlength' => true, 'id' => 'latitude'])->label(false) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div id="map" style="width:100%; height:350px"></div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'logo')->widget(FileInput::class, [
                'options' => ['accept' => 'image/*'],
                'language' => Yii::$app->language,
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => false,
                    'showRemove' => false,
                    'showCancel' => false,
                    'showUpload' => false,
                    'previewFileType' => 'any',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseLabel' => 'Юклаш',
                    'layoutTemplates' => [
                        'main1' => '<div class="kv-upload-progress hide"></div>{browse}{preview}',
                    ],
                    'initialPreview' => [
                        Html::a($model->logo, $model->getUploadedFileUrl('logo'), ['target' => '_blank'])
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    var center = ['41.315456', '69.274975'];
    if (<?= !$model->isNewRecord ? 'true' : 'false' ?>) {
        center = ['<?= $model->latitude ?>', '<?= $model->longitude ?>'];
    }
    var mapOptions = {
        center: center,
        zoom: 15
    }
    // Creating a map object
    var map = new L.map('map', mapOptions);

    // Creating a Layer object
    var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    // Adding layer to the map
    map.addLayer(layer);

    if (<?= !$model->isNewRecord ? 'true' : 'false' ?>) {
        // Creating a marker
        var marker = L.marker(['<?= $model->latitude ?>', '<?= $model->longitude ?>']);

        // Adding marker to the map
        marker.addTo(map);
    }


    var long = document.getElementById('longitude')
    var lat = document.getElementById('latitude')

    var popup = L.popup();

    function onMapClick(e) {
        long.value = e.latlng.lng
        lat.value = e.latlng.lat
        popup
            .setLatLng(e.latlng)
            .setContent("Жойлашув белгиланди")
            .openOn(map);
    }

    map.on('click', onMapClick);
</script>
