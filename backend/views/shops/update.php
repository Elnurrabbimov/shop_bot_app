<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Shops */

$this->title = 'Таҳрирлаш: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дўконлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Таҳрирлаш';
?>
<div class="categories-create">

    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
