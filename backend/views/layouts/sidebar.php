<?php

use hail812\adminlte\widgets\Menu;

?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="<?= $assetDir ?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            echo Menu::widget([
                'items' => [
                    ['label' => 'Бош саҳифа', 'icon' => 'home', 'url' => ['site/index']],
                    ['label' => 'Категориялар', 'icon' => 'list', 'url' => ['categories/index']],
                    ['label' => 'Махсулотлар', 'icon' => 'box', 'url' => ['products/index']],
                    ['label' => 'Дўконлар', 'icon' => 'hotel', 'url' => ['shops/index']],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>