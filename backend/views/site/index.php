<?php
$this->title = '';
$this->params['breadcrumbs'] = [['label' => $this->title]];
?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 style="font-size: 62px" align="center">🅰🅳🅼🅸🅽</h1>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>