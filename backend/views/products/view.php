<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Махсулотлар', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="products-view">

    <p>
        <?= Html::a('Таҳрирлаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category ? $model->category->name : '--';
                },
            ],
            'name:ntext',
            'description:ntext',
            'price',
            [
                'attribute' => 'main_image',
                'value' => function ($model) {
                    return $model->main_image ? Html::img($model->fileUrl, ['download' => true, 'style' => ['width' => '600px']]) : '--';
                },
                'format' => 'raw',
                'filter' => false,
            ],
            [
                'attribute' => 'images',
                'value' => function ($model) {
                    $photos = '';
                    foreach ($model->photos as $photo) {
                        $photos .= '<div class="row"><div class="col-md-2">
                ' . Html::a(
                                Html::img($photo->fileUrl, ['style' => ['width' => '400px']]),
                                $photo->fileUrl,
                                ['class' => 'thumbnail', 'target' => '_blank']
                            ) . '
                      </div></div><br>';
                    }
                    $photos .= '</div>';
                    return $model->photos ? $photos : '--';
                },
                'format' => 'raw',
                'filter' => false,
            ],
            'url:ntext',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
