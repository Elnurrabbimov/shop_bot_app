<?php

use common\models\Categories;
use common\models\Shops;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Танланг...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'main_image')->widget(FileInput::class, [
                'options' => ['accept' => 'image/*'],
                'language' => Yii::$app->language,
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => false,
                    'showRemove' => false,
                    'showCancel' => false,
                    'showUpload' => false,
                    'previewFileType' => 'any',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseLabel' => 'Юклаш',
                    'layoutTemplates' => [
                        'main1' => '<div class="kv-upload-progress hide"></div>{browse}{preview}',
                    ],
                    'initialPreview' => [
                        Html::a($model->main_image, $model->getUploadedFileUrl('main_image'), ['target' => '_blank'])
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'images[]')->widget(FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
