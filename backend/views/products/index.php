<?php

use common\models\Categories;
use common\models\Shops;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search_models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Махсулотлар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <p>
        <?= Html::a('Янги қўшиш', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'dataTables_wrapper dt-bootstrap4',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover dataTable dtr-inline',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category ? $model->category->name : '--';
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'category_id',
                    'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Танланг'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])

            ],
            'name:ntext',
            'description:ntext',
            'price',
            [
                'attribute' => 'main_image',
                'value' => function ($model) {
                    return $model->main_image ? Html::img($model->fileUrl, ['download' => true, 'style' => ['width' => '200px']]) : '--';
                },
                'format' => 'raw',
                'filter' => false,
            ],
            //'url:ntext',
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info"><i class="fas fa-eye"></i></span>', $url, [
                            'title' => 'Махсулотни кўриш',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-primary"><i class="fas fa-pencil"></i></span>', $url, [
                            'title' => 'Махсулотни таҳрирлаш',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="btn btn-danger"><i class="fas fa-trash"></i></span>', $url, [
                            'title' => 'Махсулотни ўчириш',
                            'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>

