<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShopProducts */
/** @var \common\models\Shops $shop */


$this->title = $model->shop->name . ' дўкони';
$this->params['breadcrumbs'][] = ['label' => $shop->name . ' дўкони махсулоти', 'url' => ['index', 'shop_id' => $shop->id]];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="shop-products-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return $model->product ? $model->product->name : '--';
                },
            ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
