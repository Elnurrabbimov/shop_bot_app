<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search_models\ShopProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/** @var \common\models\Shops $shop */


$this->title = $shop->name . ' дўкони махсулотлари';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-products-index">


    <p>
        <?= Html::a('Янги қўшиш', ['create', 'shop_id' => $shop->id], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'dataTables_wrapper dt-bootstrap4',
        ],
        'tableOptions' => [
            'class' => 'table table-bordered table-hover dataTable dtr-inline',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return $model->product ? $model->product->name : '--';
                },
            ],
            'created_at:datetime',
//            'updated_at',
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-info"><i class="fas fa-eye"></i></span>', $url, [
                            'title' => 'Дўконни кўриш',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="btn btn-danger"><i class="fas fa-trash"></i></span>', $url, [
                            'title' => 'Дўконни ўчириш',
                            'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
