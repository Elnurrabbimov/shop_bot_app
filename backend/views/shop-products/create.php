<?php


/* @var $this yii\web\View */
/* @var $model common\models\ShopProducts */
/** @var \common\models\Shops $shop */

$this->title = 'Янги қўшиш';
$this->params['breadcrumbs'][] = ['label' => $shop->name . ' дўкони махсулотлари', 'url' => ['index', 'shop_id' => $shop->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">

    <div class="card">
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
