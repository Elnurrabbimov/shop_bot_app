<?php

namespace backend\modules\controllers;

use common\models\Products;
use common\models\Shops;
use Yii;
use yii\rest\Controller;
use yii\web\Response;

class ProductsController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        parent::__construct($id, $module, $config);
    }

    public function actionProducts()
    {
        return Products::find()->asArray()->all();
    }
}