du:
	docker-compose up -d

dd:
	docker-compose down

db:
	docker-compose up --build -d

front:
	docker exec -it bot_app_frontend_1 bash

back:
	docker exec -it bot_app_backend_1 bash

baza:
	docker exec -it bot_app_postgres_1 bash

perm:
	sudo chown ${USER} console/migrations -R

dump:
	docker exec -it bot_app_postgres_1 bash -c "pg_dump --host=localhost --port=5432 --username=postgres  --dbname=bot_app > ./app/dump.sql"

restore:
	docker exec -i bot_app_postgres_1  /bin/bash -c "PGPASSWORD=bot_app psql --username postgres bot_app" > dump.sql
