<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_bot}}`.
 */
class m220622_165832_create_user_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_bot}}', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->string(),
            'category_node' => $this->string(),
            'category_page' => $this->string(),
            'shop_node' => $this->string(),
            'shop_page' => $this->string(),
            'product_node' => $this->string(),
            'product_page' => $this->string(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_bot}}');
    }
}
