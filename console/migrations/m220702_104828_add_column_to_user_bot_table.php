<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_bot}}`.
 */
class m220702_104828_add_column_to_user_bot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_bot}}', 'longitude', $this->string());
        $this->addColumn('{{%user_bot}}', 'latitude', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_bot}}', 'longitude');
        $this->dropColumn('{{%user_bot}}', 'latitude');
    }
}
