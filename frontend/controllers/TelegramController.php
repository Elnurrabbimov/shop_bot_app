<?php

namespace frontend\controllers;

use common\helpers\ConstantsHelper;
use common\helpers\Telegram;
use common\services\CategoryService;
use common\services\ProductService;
use common\services\ShopService;
use GuzzleHttp\Client;
use PDOException;
use yii\db\mssql\PDO;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Test controller
 */
class TelegramController extends Controller
{
    public $telegram;
    public $conn;
    public $enableCsrfValidation = false;
    public $categories;
    public $shops;
    public $products;

    private $hostname = 'bot_app_postgres_1';
    private $username = 'postgres';
    private $password = 'bot_app';
    private $db = 'bot_app';
    private $chat_id;

    const LIMIT = ConstantsHelper::LIMIT;

    private $bot_token = '1024199370:AAEKP-Ji8IsOuOo6fASMhCN-QIeL0nD0nOs';


    /* services */

    private $productService;
    private $categoryService;
    private $shopService;

    /* services */

    public function __construct($id, $module, $config = [])
    {
        $this->telegram = new Telegram($this->bot_token);
        try {
            $conn = new PDO("pgsql:host=$this->hostname;dbname=$this->db", $this->username, $this->password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn = $conn;

            $this->chat_id = $this->telegram->ChatID();

            $user = $this->getChatUser($this->chat_id);

            /* services */

            $this->productService = new ProductService($this->conn, $this->telegram, $user);
            $this->categoryService = new CategoryService($this->conn, $this->telegram, $user);
            $this->shopService = new ShopService($this->conn, $this->telegram, $user);

            /* services */

        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $telegram = $this->telegram;

        $data = $telegram->getData();
        $text = $telegram->Text();
        $chat_id = $telegram->ChatID();
        $message_id = $telegram->MessageID();
        $user = $this->getChatUser($this->chat_id);

        if (isset($telegram->getData()['message']['chat']['type']) && $telegram->getData()['message']['chat']['type'] != 'private')
            return;

        $startButtons = [
            [$telegram->buildKeyboardButton("📦 Махсулотлар")],
            [$telegram->buildKeyboardButton("🏠 Дўконлар")]
        ];

        $homeButton = [[$telegram->buildKeyboardButton("🔝 Бош сафиҳа")]];

        if (isset($data['callback_query'])) {
            $callback_data = $telegram->Callback_Data();
            $chat_id = $telegram->Callback_ChatID();
            if ($this->isContains($callback_data, 'category_id')) {
                $category_id = explode('=', $callback_data)[1];
                $this->categoryService->setCategoryNode($category_id);
                $this->products = $this->productService->getProducts($category_id);
                if (!empty($this->products)) {
                    $this->productService->renderProducts($this->products, $message_id);
                } else {
                    $keyb = $telegram->buildKeyBoard($homeButton, $onetime = true, $resize = true);
                    $telegram->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => 'Ушбу категорияга оид махсулот топилмади',
                        'reply_markup' => $keyb,
                    ]);
                }
            } elseif ($this->isContains($callback_data, 'shop_id')) {
                $shop_id = explode('=', $callback_data)[1];
                $this->shopService->setShopNode($shop_id);
                $shop = $this->shopService->getShop($shop_id);
                $this->shopService->renderShop($shop, $message_id);
            } elseif ($this->isContains($callback_data, 'product_id')) {
                $product_id = explode('=', $callback_data)[1];
                $product = $this->productService->getProduct($product_id);
                if ($product) {
                    $this->productService->renderProduct($product);
                } else {
                    $keyb = $telegram->buildKeyBoard($homeButton, $onetime = true, $resize = true);
                    $telegram->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => 'Ушбу махсулот топилмади',
                        'reply_markup' => $keyb,
                    ]);
                }
            } elseif ($this->isContains($callback_data, 'next_category')) {
                $offset = $this->categoryService->setNextPage();
                $this->categories = $this->categoryService->getCategories(self::LIMIT, $offset);
                $this->categoryService->renderCategories($this->categories, $message_id);
            } elseif ($this->isContains($callback_data, 'previous_category')) {
                $offset = $this->categoryService->setPreviousPage();
                $this->categories = $this->categoryService->getCategories(self::LIMIT, $offset);
                $this->categoryService->renderCategories($this->categories, $message_id);
            } elseif ($this->isContains($callback_data, 'next_shop')) {
                $offset = $this->shopService->setNextPage();
                $this->shops = $this->shopService->getShops(self::LIMIT, $offset);
                $this->shopService->renderShops($this->shops, $message_id);
            } elseif ($this->isContains($callback_data, 'previous_shop')) {
                $offset = $this->shopService->setPreviousPage();
                $this->shops = $this->shopService->getShops(self::LIMIT, $offset);
                $this->shopService->renderShops($this->shops, $message_id);
            } elseif ($this->isContains($callback_data, 'next_product')) {
                $offset = $this->productService->setNextPage();
                $this->products = $this->productService->getProducts($user['category_node'], self::LIMIT, $offset);
                $this->productService->renderProducts($this->products, $message_id);
            } elseif ($this->isContains($callback_data, 'previous_product')) {
                $offset = $this->productService->setPreviousPage();
                $this->products = $this->productService->getProducts($user['category_node'], self::LIMIT, $offset);
                $this->productService->renderProducts($this->products, $message_id);
            } elseif ($this->isContains($callback_data, 'show_location')) {
                $shop_id = explode('=', $callback_data)[1];
                $shop = $this->shopService->getShop($shop_id);
                $telegram->sendLocation([
                    'chat_id' => $chat_id,
                    'latitude' => $shop['latitude'],
                    'longitude' => $shop['longitude']
                ]);
            } elseif ($this->isContains($callback_data, 'show_shop_products')) {
                $shop_id = explode('=', $callback_data)[1];
                $this->products = $this->shopService->getShopProducts($shop_id);
                if (!empty($this->products)) {
                    $this->shopService->renderProducts($this->products);
                } else {
                    $keyb = $telegram->buildKeyBoard($homeButton, $onetime = true, $resize = true);
                    $telegram->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => 'Ушбу дўконда махсулотлар топилмади',
                        'reply_markup' => $keyb,
                    ]);
                }
            } elseif ($this->isContains($callback_data, 'show_product_images')) {
                $product_id = explode('=', $callback_data)[1];
                $productImages = $this->productService->getProductImages($product_id);
                if (!empty($productImages)) {
                    $this->productService->renderProductImages($productImages);
                } else {
                    $keyb = $telegram->buildKeyBoard($homeButton, $onetime = true, $resize = true);
                    $telegram->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => 'Қўшимча расмлар мавжуд эмас',
                        'reply_markup' => $keyb,
                    ]);
                }
            } elseif ($this->isContains($callback_data, 'change_location')) {
                $keyb = $telegram->buildKeyBoard([
                    [$telegram->buildKeyboardButton("📍 Жорий жойлашувни юбориш", false, true)],
                    [$telegram->buildKeyboardButton("❌ Локацияни юбора олмайман")]
                ], $onetime = true, $resize = true);
                $content = array('chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Сизга яқин орадаги дўконларни кўриш учун локацияни юборинг');
                $telegram->sendMessage($content);
            } elseif ($this->isContains($callback_data, 'product_isset_shop')) {
                $product_id = explode('=', $callback_data)[1];
                $shops = $this->shopService->searchShopsByProduct($product_id);
                if (!empty($shops)) {
                    $this->shopService->renderShopsBySearch($shops);
                } else {
                    $keyb = $telegram->buildKeyBoard($homeButton, $onetime = true, $resize = true);
                    $telegram->sendMessage([
                        'chat_id' => $chat_id,
                        'text' => 'Ушбу махсулот дўконларда топилмади',
                        'reply_markup' => $keyb,
                    ]);
                }
            }
        }

        if (isset($data['message'])) {
            $message = $data['message'];
            if (isset($message['location']['latitude']) && isset($message['location']['longitude'])) {
                $this->setLocation($message['location']['latitude'], $message['location']['longitude']);
                $this->shops = $this->shopService->getShops();
                $keyb = $telegram->buildKeyBoard($startButtons, $onetime = true, $resize = true);
                $telegram->sendMessage([
                    'chat_id' => $chat_id,
                    'text' => 'Жойлашувингиз мувоффақиятли белгиланди.Қуйидаги бўлимлардан бирини танланг',
                    'reply_markup' => $keyb
                ]);
            }
        }

        if ($text == '/start' || $text == '🔝 Бош сафиҳа') {
            $keyb = $telegram->buildKeyBoard($startButtons, $onetime = true, $resize = true);
            $content = array('chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Қуйидаги бўлимлардан бирини танланг');
            $telegram->sendMessage($content);
        } elseif ($text == '📦 Махсулотлар') {
            $this->categoryService->setDefPage();
            $this->categories = $this->categoryService->getCategories();
            $this->categoryService->renderCategories($this->categories);
        } elseif ($text == '🏠 Дўконлар') {
            $this->shopService->setDefPage();
            if (isset($user['longitude']) && $user['longitude']) {
                $this->shops = $this->shopService->getShops();
                $this->shopService->renderShops($this->shops);
            } else {
                $keyb = $telegram->buildKeyBoard([
                    [$telegram->buildKeyboardButton("📍 Жорий жойлашувни юбориш", false, true)],
                    [$telegram->buildKeyboardButton("❌ Локацияни юбора олмайман")]
                ], $onetime = true, $resize = true);
                $content = array('chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Сизга яқин орадаги дўконларни кўриш учун локацияни юборинг');
                $telegram->sendMessage($content);
            }
        } elseif ($text == '❌ Локацияни юбора олмайман') {
            $this->setLocation(-1, -1);
            $keyb = $telegram->buildKeyBoard($startButtons, $onetime = true, $resize = true);
            $content = array('chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => 'Қуйидаги бўлимлардан бирини танланг');
            $telegram->sendMessage($content);
        }

    }


    public function isContains($string, $needle)
    {
        return strpos($string, $needle) !== false;
    }

    private function getChatUser($chat_id)
    {
        $user = $this->conn->prepare('SELECT * FROM user_bot WHERE chat_id=:chat_id');
        $user->bindParam(':chat_id', $chat_id);
        $user->execute();
        $data = $user->fetch();
        if (!$data) {
            $user = $this->conn->prepare('INSERT INTO user_bot(chat_id) VALUES (:chat_id)');
            $user->bindParam(':chat_id', $chat_id);
            $user->execute();

            $user = $this->conn->prepare('SELECT * FROM user_bot WHERE chat_id=:chat_id');
            $user->bindParam(':chat_id', $chat_id);
            $user->execute();
            $data = $user->fetch();
        }

        return $data;
    }

    private function setLocation($lat, $lng)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET latitude=:lat, longitude=:lng WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':lat', $lat);
        $model->bindParam(':lng', $lng);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
        return false;
    }

}
