<?php

namespace common\helpers;

use Yii;

class FileHelper
{
    public static function saveProductImage($file, $model)
    {
        $dir = Yii::getAlias('@storage/data/product-images/' . $model->id);

        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $postdata = fopen($file->tempName, "r");

        $ext = pathinfo($file->name, PATHINFO_EXTENSION);

        $filename = $dir . '/' . $file->name;

        $fp = fopen($filename, "w");

        while ($data = fread($postdata, 1024))
            fwrite($fp, $data);

        fclose($fp);
        fclose($postdata);

        return $filename;
    }
}