<?php

namespace common\services;


use common\helpers\ConstantsHelper;
use yii\helpers\VarDumper;

class CategoryService
{
    private $conn;
    private $telegram;

    public $chat_id;
    public $message_id;
    public $text;
    public $user_id;
    public $data;
    public $user;


    public function __construct($conn, $telegram, $user)
    {
        $this->conn = $conn;
        $this->telegram = $telegram;
        $this->user = $user;

        $this->data = $telegram->getData();
        $this->text = $telegram->Text();
        $this->chat_id = $telegram->ChatID();
        $this->message_id = $telegram->MessageID();
    }

    public function getCategories($limit = ConstantsHelper::LIMIT, $offset = 0)
    {
        $sql = "SELECT * FROM categories ORDER BY id ASC LIMIT {$limit} OFFSET {$offset}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $categories = $stmt->fetchAll();
        return $categories;
    }

    public function countCategorys()
    {
        $sql = "SELECT COUNT(*) FROM categories";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        return $count;
    }

    public function renderCategories($categories, $messageId = null)
    {
        $telegram = $this->telegram;
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $buttons[] = [$telegram->buildInlineKeyBoardButton($category['name'], '', 'category_id=' . $category['id'])];
            }

            $buttons[] = [$telegram->buildInlineKeyBoardButton('◀️ Олдин', '', 'previous_category=' . $category['id']),
                $telegram->buildInlineKeyBoardButton('Кейин ▶️', '', 'next_category=' . $category['id'])];

            $keyb = $telegram->buildInlineKeyBoard($buttons, $onetime = true, $resize = true);

            if (!$messageId) {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Категориялар');
                $telegram->sendMessage($content);
            } else {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Категориялар', 'message_id' => $messageId);
                $telegram->editMessageText($content);
            }
        }
    }


    public function setCategoryNode($id)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET category_node=:category_node WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':category_node', $id);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
        return false;
    }

    public function setNextPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage + ConstantsHelper::LIMIT;
        if ($nextPage >= $this->countCategorys())
            return $currentPage;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function setPreviousPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage - ConstantsHelper::LIMIT;
        if ($nextPage < 0)
            $nextPage = 0;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function getPage()
    {
        $model = $this->conn->prepare("SELECT category_page FROM user_bot WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->execute();
        $page = $model->fetch();
        return $page['category_page'];
    }

    public function setPage($page)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET category_page=:category_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':category_page', $page);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

    public function setDefPage()
    {
        $defPage = '0';
        $model = $this->conn->prepare("UPDATE user_bot SET category_page=:category_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':category_page', $defPage);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

}