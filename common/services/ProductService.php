<?php

namespace common\services;


use common\helpers\ConstantsHelper;
use CURLFile;

class ProductService
{
    private $conn;
    private $telegram;

    public $chat_id;
    public $message_id;
    public $text;
    public $user_id;
    public $data;


    public function __construct($conn, $telegram, $user)
    {
        $this->conn = $conn;
        $this->telegram = $telegram;

        $this->data = $telegram->getData();
        $this->text = $telegram->Text();
        $this->chat_id = $telegram->ChatID();
        $this->message_id = $telegram->MessageID();
    }


    public function getProducts($category_id, $limit = ConstantsHelper::LIMIT, $offset = 0)
    {
        $sql = "SELECT * FROM products WHERE category_id = {$category_id} ORDER BY id DESC LIMIT {$limit} OFFSET {$offset}";


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $products = $stmt->fetchAll();
        return $products;
    }

    public function renderProducts($products, $messageId = null)
    {
        $telegram = $this->telegram;
        if (!empty($products)) {
            foreach ($products as $product) {
                $buttons[] = [$telegram->buildInlineKeyBoardButton($product['name'], '', 'product_id=' . $product['id'])];
            }

            $buttons[] = [$telegram->buildInlineKeyBoardButton('◀️ Олдин', '', 'previous_product=' . $product['id']),
                $telegram->buildInlineKeyBoardButton('Кейин ▶️', '', 'next_product=' . $product['id'])];

            $keyb = $telegram->buildInlineKeyBoard($buttons, $onetime = true, $resize = true);

            if (!$messageId) {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Махсулотлар');
                $telegram->sendMessage($content);
            } else {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Махсулотлар', 'message_id' => $messageId);
                $telegram->editMessageText($content);
            }
        }
    }

    public function getProduct($id)
    {
        $sql = "SELECT * FROM products WHERE id = $id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $product = $stmt->fetch();
        return $product;
    }

    public function renderProduct($product, $messageId = null)
    {
        $telegram = $this->telegram;
        $keyb = $telegram->buildInlineKeyBoard([
            [$telegram->buildInlineKeyboardButton("🏞 Қўшимча расмларни кўриш", '', 'show_product_images=' . $product['id'])],
            [$telegram->buildInlineKeyboardButton("🔍 Ушбу махсулот мавжуд дўконлар", '', 'product_isset_shop=' . $product['id'])],
        ], $onetime = true, $resize = true);

        $productCaption = 'Номи: ' . $product['name'] . PHP_EOL .
            'Маълумот: ' . $product['description'] . PHP_EOL .
            'Нархи: ' . $product['price'] . PHP_EOL .
            'Сана: ' . date('d.m.Y', strtotime($product['created_at'])) . PHP_EOL .
            'Линк: ' . $product['url'];

        $photo = ConstantsHelper::SITE_URL . '/uploads/storage/data/products/' . $product['id'] . '/' . $product['main_image'];
        $content = [
            'chat_id' => $this->chat_id,
            'photo' => new CURLFile($photo),
            'caption' => $productCaption,
            'parse_mode' => 'HTML',
            'reply_markup' => $keyb
        ];
        $telegram->sendPhoto($content);
    }

    public function getProductImages($product_id)
    {
        $sql = "SELECT * FROM product_images WHERE product_id = {$product_id}";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $images = $stmt->fetchAll();
        return $images;
    }

    public function renderProductImages($productImages)
    {
        $telegram = $this->telegram;
        $items = $productImages;
        if (!empty($items)) {
            foreach ($items as $image) {
                $photo = ConstantsHelper::SITE_URL . '/uploads/storage/data/product-images/' . $image['id'] . '/' . $image['image'];
                $content = [
                    'chat_id' => $this->chat_id,
                    'photo' => new CURLFile($photo),
                    'caption' => '',
                    'parse_mode' => 'HTML',
                ];
                $telegram->sendPhoto($content);
            }
        }
    }

    public function setDefPage()
    {
        $defPage = '0';
        $model = $this->conn->prepare("UPDATE user_bot SET product_page=:product_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':product_page', $defPage);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

    public function setNextPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage + ConstantsHelper::LIMIT;
        if ($nextPage >= $this->countProducts())
            return $currentPage;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function setPreviousPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage - ConstantsHelper::LIMIT;
        if ($nextPage < 0)
            $nextPage = 0;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function getPage()
    {
        $model = $this->conn->prepare("SELECT product_page FROM user_bot WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->execute();
        $page = $model->fetch();
        return $page['product_page'];
    }

    public function setPage($page)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET product_page=:product_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':product_page', $page);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

    public function countProducts()
    {
        $sql = "SELECT COUNT(*) FROM shops";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        return $count;
    }

}