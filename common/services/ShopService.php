<?php

namespace common\services;

use common\helpers\ConstantsHelper;
use common\helpers\Telegram;
use CURLFile;
use yii\helpers\VarDumper;

class ShopService
{
    private $conn;
    private $telegram;

    public $chat_id;
    public $message_id;
    public $text;
    public $user_id;
    public $data;
    public $user;


    public function __construct($conn, $telegram, $user)
    {
        $this->conn = $conn;
        $this->telegram = $telegram;
        $this->user = $user;

        $this->data = $telegram->getData();
        $this->text = $telegram->Text();
        $this->chat_id = $telegram->ChatID();
        $this->message_id = $telegram->MessageID();
    }

    public function setShopNode($id)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET shop_node=:shop_node WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':shop_node', $id);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
        return false;
    }

    public function getShop($id)
    {
        $sql = "SELECT * FROM shops WHERE id = $id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $shop = $stmt->fetch();
        return $shop;
    }

    public function renderShop($shop, $messageId = null)
    {
        $telegram = $this->telegram;
        $keyb = $telegram->buildInlineKeyBoard([
            [$telegram->buildInlineKeyboardButton("🗾 Дўкон манзилини юбориш", '', 'show_location=' . $shop['id'])],
            [$telegram->buildInlineKeyboardButton("📦 Дўконда мавжуд махсулотлар", '', 'show_shop_products=' . $shop['id'])],
        ], $onetime = true, $resize = true);

        $shopCaption = 'Номи: ' . $shop['name'] . PHP_EOL .
            'Маълумот: ' . $shop['description'];

        $photo = ConstantsHelper::SITE_URL . '/uploads/storage/data/shop-image/' . $shop['id'] . '/' . $shop['logo'];

        $content = [
            'chat_id' => $this->chat_id,
            'photo' => new CURLFile($photo),
            'caption' => $shopCaption,
            'parse_mode' => 'HTML',
            'reply_markup' => $keyb
        ];
        $telegram->sendPhoto($content);
    }

    public function getShops($limit = ConstantsHelper::LIMIT, $offset = 0)
    {
        $user = $this->user;
        if ($user['longitude'] == -1 || $user['longitude'] == null) {
            $sql = "SELECT * FROM shops ORDER BY id DESC LIMIT $limit OFFSET $offset";
        } else {
            $sql = "SELECT *  FROM (
        SELECT * ,(3956 * 2 * ASIN(SQRT( POWER(SIN(({$user['latitude']} - shops.latitude) *  pi()/180 / 2), 2) + COS( {$user['latitude']} * pi()/180) * COS(latitude * pi()/180) * POWER(SIN(({$user['longitude']} - longitude) * pi()/180 / 2), 2) )))  AS distance
        FROM shops ) al
        where  distance < 1000
        order by distance
        LIMIT {$limit} OFFSET {$offset}";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $shops = $stmt->fetchAll();
        return $shops;
    }

    public function renderShops($shops, $messageId = null)
    {
        $telegram = $this->telegram;
        if (!empty($shops)) {
            foreach ($shops as $shop) {
                $buttons[] = [$telegram->buildInlineKeyBoardButton($shop['name'], '', 'shop_id=' . $shop['id'])];
            }
            $buttons[] = [$telegram->buildInlineKeyBoardButton('◀️ Олдин', '', 'previous_shop=' . $shop['id']), $telegram->buildInlineKeyBoardButton('Кейин ▶️', '', 'next_shop=' . $shop['id'])];
            $buttons[] = [$telegram->buildInlineKeyBoardButton("🛠 Жойлашувни қайтадан белгилаш", '', 'change_location')];

            $keyb = $telegram->buildInlineKeyBoard($buttons, $onetime = true, $resize = true);
            if (!$messageId) {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Дўконлар');
                $telegram->sendMessage($content);
            } else {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Дўконлар', 'message_id' => $messageId);
                $telegram->editMessageText($content);
            }
        }
    }

    public function renderShopsBySearch($shops, $messageId = null)
    {
        $telegram = $this->telegram;
        if (!empty($shops)) {
            foreach ($shops as $shop) {
                $buttons[] = [$telegram->buildInlineKeyBoardButton($shop['name'], '', 'shop_id=' . $shop['id'])];
            }

            $keyb = $telegram->buildInlineKeyBoard($buttons, $onetime = true, $resize = true);
            if (!$messageId) {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Дўконлар');
                $telegram->sendMessage($content);
            } else {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Дўконлар', 'message_id' => $messageId);
                $telegram->editMessageText($content);
            }
        }
    }

    public function setDefPage()
    {
        $defPage = '0';
        $model = $this->conn->prepare("UPDATE user_bot SET shop_page=:shop_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':shop_page', $defPage);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

    public function setNextPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage + ConstantsHelper::LIMIT;
        if ($nextPage >= $this->countShops())
            return $currentPage;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function setPreviousPage()
    {
        $currentPage = $this->getPage();
        $nextPage = $currentPage - ConstantsHelper::LIMIT;
        if ($nextPage < 0)
            $nextPage = 0;

        $this->setPage($nextPage);
        $currentPage = $this->getPage();
        return $currentPage;
    }

    public function getPage()
    {
        $model = $this->conn->prepare("SELECT shop_page FROM user_bot WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->execute();
        $page = $model->fetch();
        return $page['shop_page'];
    }

    public function setPage($page)
    {
        $model = $this->conn->prepare("UPDATE user_bot SET shop_page=:shop_page WHERE chat_id=:chat_id");
        $model->bindParam(':chat_id', $this->chat_id);
        $model->bindParam(':shop_page', $page);
        $model->execute();
        if ($model->rowCount() > 0)
            return true;
    }

    public function countShops()
    {
        $sql = "SELECT COUNT(*) FROM shops";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        return $count;
    }

    public function getShopProducts($shopId)
    {
        $sql = "SELECT * FROM shop_products
         INNER JOIN products ON shop_products.product_id = products.id
         WHERE shop_id = :shop_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':shop_id', $shopId);
        $stmt->execute();
        $products = $stmt->fetchAll();
        return $products;
    }

    public function renderProducts($products, $messageId = null)
    {
        $telegram = $this->telegram;
        if (!empty($products)) {
            foreach ($products as $product) {
                $buttons[] = [$telegram->buildInlineKeyBoardButton($product['name'], '', 'product_id=' . $product['id'])];
            }

            $keyb = $telegram->buildInlineKeyBoard($buttons, $onetime = true, $resize = true);

            if (!$messageId) {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Махсулотлар');
                $telegram->sendMessage($content);
            } else {
                $content = array('chat_id' => $this->chat_id, 'reply_markup' => $keyb, 'text' => 'Махсулотлар', 'message_id' => $messageId);
                $telegram->editMessageText($content);
            }
        }
    }

    public function searchShopsByProduct($id)
    {
        $sql = "SELECT * FROM shop_products
         INNER JOIN shops ON shop_products.shop_id = shops.id
         WHERE product_id = :product_id";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':product_id', $id);
        $stmt->execute();
        $shops = $stmt->fetchAll();
        return $shops;
    }
}