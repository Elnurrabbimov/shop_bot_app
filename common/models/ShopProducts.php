<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "shop_products".
 *
 * @property int $id
 * @property int|null $shop_id
 * @property int|null $product_id
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ShopProducts extends ActiveRecord
{
    public $products;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_products';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => date('Y-m-d H:i:s')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
//            [['shop_id', 'product_id'], 'unique', 'targetAttribute' => ['shop_id', 'product_id']],
            [['shop_id', 'product_id'], 'default', 'value' => null],
            [['shop_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at', 'products'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop_id' => 'Дўкон',
            'product_id' => 'Махсулот',
            'products' => 'Маълумотлар тўплами',
            'created_at' => 'Қўшилган вақти',
            'updated_at' => 'Ўзгартирилган вақти',
        ];
    }


    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    public function getShop()
    {
        return $this->hasOne(Shops::class, ['id' => 'shop_id']);
    }

}
