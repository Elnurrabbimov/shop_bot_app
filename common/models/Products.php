<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $main_image
 * @property string|null $price
 * @property string|null $url
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Products extends ActiveRecord
{
    public $images;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => date('Y-m-d H:i:s')
            ],
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'main_image',
                'filePath' => '@storage/data/products/[[attribute_id]]/[[filename]].[[extension]]',
                'fileUrl' => '/uploads/storage/data/products/[[attribute_id]]/[[filename]].[[extension]]'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'main_image'], 'required'],
            [['category_id'], 'default', 'value' => null],
            [['category_id'], 'integer'],
            [['name', 'description', 'url'], 'string'],
            [['created_at', 'updated_at', 'main_image', 'images'], 'safe'],
            [['main_image'], 'file', 'maxSize' => 1024 * 1024 * 1, 'tooBig' => 'Файл не должен превышать 1 мб'],
            ['url', 'url'],
            ['images', 'each', 'rule' => ['image', 'maxSize' => 1024 * 1024 * 1, 'tooBig' => 'Файл не должен превышать 1 мб']],
            [['price'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Номи',
            'description' => 'Тавсиф',
            'main_image' => 'Асосий расм',
            'price' => 'Нархи',
            'images' => 'Расмлар',
            'url' => 'Сайт',
            'created_at' => 'Қўшилган вақти',
            'updated_at' => 'Ўзгартирилган вақти',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }

    public function getPhotos()
    {
        return $this->hasMany(ProductImages::class, ['product_id' => 'id']);
    }

    public function getFileUrl()
    {
        return $this->getUploadedFileUrl('main_image');
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->images = UploadedFile::getInstances($this, 'images');
            return true;
        }
        return false;
    }
}
