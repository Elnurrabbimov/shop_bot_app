<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "shops".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $logo
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Shops extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => date('Y-m-d H:i:s')
            ],
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'logo',
                'filePath' => '@storage/data/shop-image/[[attribute_id]]/[[filename]].[[extension]]',
                'fileUrl' => '/uploads/storage/data/shop-image/[[attribute_id]]/[[filename]].[[extension]]'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'longitude', 'latitude', 'logo'], 'required'],
            [['description'], 'string'],
            [['logo'], 'file', 'maxSize' => 1024 * 1024 * 1, 'tooBig' => 'Файл не должен превышать 1 мб'],
            [['longitude', 'latitude'], 'string'],
            [['created_at', 'updated_at', 'location', 'logo'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Дўкон номи',
            'description' => 'Дўкон тавсифи',
            'location' => 'Дўкон манзили',
            'logo' => 'Дўкон расми',
            'created_at' => 'Қўшилган вақти',
            'updated_at' => 'Ўзгартирилган вақти',
            'longitude' => 'Узунлик(longitude)',
            'latitude' => 'Кенглик(latitude)',
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['shop_id' => 'id']);
    }

    public function getFileUrl()
    {
        return $this->getUploadedFileUrl('logo');
    }
}
