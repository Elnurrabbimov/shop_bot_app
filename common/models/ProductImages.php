<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yiidreamteam\upload\FileUploadBehavior;

/**
 * This is the model class for table "product_images".
 *
 * @property int $id
 * @property int|null $product_id
 * @property string|null $image
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ProductImages extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_images';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                'value' => date('Y-m-d H:i:s')
            ],
            [
                'class' => FileUploadBehavior::class,
                'attribute' => 'image',
                'filePath' => '@storage/data/product-images/[[attribute_id]]/[[filename]].[[extension]]',
                'fileUrl' => '/uploads/storage/data/product-images/[[attribute_id]]/[[filename]].[[extension]]'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'default', 'value' => null],
            [['product_id'], 'integer'],
            [['image'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Махсулот',
            'image' => 'Расм',
            'created_at' => 'Қўшилган вақти',
            'updated_at' => 'Ўзгартирилган вақти',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getFileUrl()
    {
        return $this->getUploadedFileUrl('image');
    }
}
