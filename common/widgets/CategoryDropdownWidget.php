<?php
namespace common\widgets;

use yii\base\Widget;

class CategoryDropdownWidget extends Widget
{
    public $column;
    public $className;
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('categoryDropdownWidget',[
            'column' => $this->column,
            'className' => $this->className
        ]);
    }
}
