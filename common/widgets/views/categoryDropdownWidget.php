<?php


use common\models\Categories;
use yii\web\View;

/** @var String $column */
$lang = '';
$maps = Categories::find()->asArray()->all();
$datas = [];
foreach ($maps as $map) {
    $datas[] = [
        'id' => $map['id'],
        'parent_id' => $map['parent_id'] ?? 0,
        'label' => $map['name']
    ];
}

function buildtree($src_arr, $parent_id = 0, $tree = array())
{
    foreach ($src_arr as $idx => $row) {
        if ($row['parent_id'] == $parent_id) {
            foreach ($row as $k => $v)
                $tree[$row['id']][$k] = $v;
            unset($src_arr[$idx]);
            $tree[$row['id']]['child'] = buildtree($src_arr, $row['id']);
        }
    }
    ksort($tree);
    return $tree;
}

$treeItems = buildtree($datas);
$treeItemsJson = json_encode($treeItems);
function getNested($items)
{
    $data = '';
    foreach ($items as $value) {
        $value['label'] = str_replace('"', '`', $value['label']);
        $changeValue = $value['id'] . "," . "'" . $value['label'] . "'";
        if (empty($value['child'])) {
            $data .= '<div class="menu-item px-3" onclick="changeValue(' . $changeValue . ')">
                            <span class="menu-link px-3">
                                ' . $value["label"] . '
                            </span>
                      </div>';
        } else {
            $data .= '<div class="menu-item px-3">
                <div class="menu-link px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                        <span onclick="changeValue(' . $changeValue . ')">    
                             ' . $value["label"] . '
                        </span>
                       &nbsp; <span class="fa fa-arrow-right"></span>
                </div>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">';
            if (!empty($value['child'])) {
                $data .= getNested($value['child']);
            }
            $data .= '
                </div>
            </div>';
        }
    }
    return $data;
}

?>

<div>
    <button type="button" class="btn btn-primary btn-sm form-control form-control-solid <?= $className ?? '' ?>"
            data-kt-menu-trigger="click"
            data-kt-menu-placement="bottom-start"
            id="departmentBtn"
    >
        Select category
    </button>

    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4"
         data-kt-menu="true">
        <?= getNested($treeItems) ?>
    </div>
</div>

<?php
$js = <<<JS
    let column = '{$column}';
    let treeItems = {$treeItemsJson};
    let input = document.getElementById(column)
    let button = document.getElementById('departmentBtn')
    function changeValue(id, label) {
       button.innerHTML = label
       input.value = id
    }
    
    window.addEventListener('DOMContentLoaded', (event) => {
        if (input.value){
              loadXMLDoc(input.value)
        }
       function loadXMLDoc(value) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {  
               if (xmlhttp.status == 200) {
                   let res = JSON.parse(xmlhttp.responseText);
                   button.innerHTML = res['name_' + '{$lang}']
               }
            }
        };
    
        xmlhttp.open("GET", "/department/find?id=" + value, true);
        xmlhttp.send();
}
});
JS;


$this->registerJs($js, View::POS_END);
?>

